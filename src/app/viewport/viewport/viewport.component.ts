import {
  Component,
  OnInit,
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { AppState } from 'src/app/app.state'
import { SetError } from 'src/app/WebGL/actions';


import { Shaders } from '../shaders/shaders.service';

import { ShaderInfo, ShadersList } from '../shaders/shaders.source';

import * as glMatrix from 'gl-matrix';

interface ProgramInfo {
  attributeLocations?: {
    [key: string]: number
  },
  uniformLocations?: {
    [key: string]: WebGLUniformLocation
  }
}


@Component({
  selector: 'viewport',
  templateUrl: './viewport.component.html',
  styleUrls: ['./viewport.component.css']
})
export class ViewportComponent {
  private subscriptions: Subscription[];
  private glContext: WebGLRenderingContext;
  private projectionMatrix: any;


  protected shaderProgram: WebGLProgram;
  protected programInfo: ProgramInfo;

  private rotation = 0;
  private activeTimestamp = 0;


  private image: any;


  constructor(
      private shaders:Shaders,
      private store: Store<AppState>
  ) {

    this.subscriptions = [
    	store.pipe(
    	  select('webgl', 'context'),
    	  first(ctx => ctx != null)
    	).subscribe(this.init.bind(this)),
    ];
  }

  async init(ctx: WebGLRenderingContext) {
  	this.glContext = ctx;

  	//await this.loadTextures();

  	const shaderInfos: ShaderInfo[] = [
  		ShadersList.VERTEX_DEFAULT,
  		ShadersList.FRAGMENT_DEFAULT,
  	]

		this.shaderProgram = this.shaders.initShaderProgram(this.glContext, shaderInfos)

  	this.setProgramInfo(shaderInfos);

		this.createBuffer();
		this.resetCanvas();
		  	// Tell WebGL to use our program when drawing
				this.glContext.useProgram(this.shaderProgram);
  	requestAnimationFrame(this.render.bind(this));
  }


  setProgramInfo(shaderInfoList: ShaderInfo[]) {

  	const attributeLocations: {[key: string]: number} = {};
  	const uniformLocations: {[key: string]: WebGLUniformLocation} = {} ;


		shaderInfoList.forEach( (curInfo: ShaderInfo) => {

			for (let curUniformKey in curInfo.uniforms) {
				const curUniformName: string = curInfo.uniforms[curUniformKey];
				uniformLocations[curUniformKey] = this.glContext.getUniformLocation(this.shaderProgram, curUniformName);
			}


			for (let curAttributeKey in curInfo.attributes) {
				const curAttributeName: string = curInfo.attributes[curAttributeKey];
				attributeLocations[curAttributeKey] = this.glContext.getAttribLocation(this.shaderProgram, curAttributeName);
			}

		});

    this.programInfo = { attributeLocations, uniformLocations };
  }

  drawScene() {
		// Clear the canvas before we start drawing on it.
  	this.glContext.clear(this.glContext.COLOR_BUFFER_BIT | this.glContext.DEPTH_BUFFER_BIT);



    //set projection matrix to deal with aspect ratio
  	this.setProjectionMatrix()

/*   	this.glContext.activeTexture(this.glContext.TEXTURE0);
  	this.glContext.uniform1i(this.programInfo.uniformLocations.sampler, 0); */

    {
      const offset = 0;
      const vertexCount = 6;
      //this.glContext.drawArrays(this.glContext.TRIANGLE_STRIP, offset, vertexCount);
      this.glContext.drawElements(this.glContext.TRIANGLES, vertexCount, this.glContext.UNSIGNED_SHORT, offset);
    }
  }


  resetCanvas() {
  	this.glContext.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    this.glContext.clearDepth(1.0);                 // Clear everything
    this.glContext.enable(this.glContext.DEPTH_TEST);           // Enable depth testing
    this.glContext.depthFunc(this.glContext.LEQUAL);            // Near things obscure far things    
  }

  setProjectionMatrix() {
  	const projectionMatrix/*:glMatrix.mat4 ???*/ = glMatrix.mat4.create();

		//@TODO the firsts value of the vector is the aspect ratio of the display.  Should be an input value for the component.
  	//glMatrix.mat4.fromScaling(projectionMatrix, [0.5, 1, 1])

  	//@TODO trying to get ortho view working
  	glMatrix.mat4.ortho(projectionMatrix, -2, 2, -1, 1, 0, 100);

  	//console.log("projection matrix scaled?", projectionMatrix);

  	this.glContext.uniformMatrix4fv(
        this.programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix)

    const modelViewMatrix = glMatrix.mat4.create();
    //glMatrix.mat4.fromRotation(modelViewMatrix, this.rotation, [0, 0, 1])
    glMatrix.mat4.fromRotation(modelViewMatrix, 0, [0, 0, 1])

    this.glContext.uniformMatrix4fv(
    	this.programInfo.uniformLocations.modelViewMatrix,
    	false,
    	modelViewMatrix
    );
  }



  createBuffer(): void {
  	// Now create an array of positions for the square.
    const positions = [
       -0.5, 0.5,
       0.5, 0.5,
       -0.5, -0.5,
       0.5, -0.5,
/*       0.5, -0.5,
        -0.5, -0.5,
        0.5, 0.5,
        -0.5, 0.5,*/

    ];

  	// Create a buffer for the square's positions.

    const positionBuffer = this.glContext.createBuffer();
    // Select the positionBuffer as the one to apply buffer
    // operations to from here out.

    this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, positionBuffer);



    // Now pass the list of positions into WebGL to build the
    // shape. We do this by creating a Float32Array from the
    // JavaScript array, then use it to fill the current buffer.

    this.glContext.bufferData(
    	this.glContext.ARRAY_BUFFER,
      new Float32Array(positions),
      this.glContext.DYNAMIC_DRAW
    );

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute.
    {
      const numComponents = 2;  // pull out 2 values per iteration
      const type = this.glContext.FLOAT;    // the data in the buffer is 32bit floats
      const normalize = false;  // don't normalize
      const stride = 0;         // how many bytes to get from one set of values to the next
                                // 0 = use type and numComponents above
      const offset = 0;         // how many bytes inside the buffer to start from
      this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, positionBuffer);
      this.glContext.vertexAttribPointer(
          this.programInfo.attributeLocations.vertexPosition,
          numComponents,
          type,
          normalize,
          stride,
          offset);
      this.glContext.enableVertexAttribArray(
          this.programInfo.attributeLocations.vertexPosition
      );
    }

	  //component buffer?
	  const indices: number[] = [
	  	0,1,2, 1,2,3
	  ];

		const indexBuffer = this.glContext.createBuffer();
  	this.glContext.bindBuffer(this.glContext.ELEMENT_ARRAY_BUFFER, indexBuffer);
	  this.glContext.bufferData(this.glContext.ELEMENT_ARRAY_BUFFER,
      new Uint16Array(indices), this.glContext.STATIC_DRAW);

    /* const colors = [
	    1.0,  1.0,  1.0,  1.0,    // white
	    1.0,  0.0,  0.0,  1.0,    // red
	    0.0,  1.0,  0.0,  1.0,    // green
	    0.0,  0.0,  1.0,  1.0,    // blue
	  ];
 */

		const colors = [
			1.0,  0.0,  0.0,  1.0,   // red
			1.0,  0.0,  0.0,  1.0,   // red
			1.0,  0.0,  0.0,  1.0,   // red
			1.0,  0.0,  0.0,  1.0,   // red
		];

	  const colorBuffer = this.glContext.createBuffer();
	  this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, colorBuffer);
	  this.glContext.bufferData(this.glContext.ARRAY_BUFFER, new Float32Array(colors), this.glContext.STATIC_DRAW);

	  // Tell WebGL how to pull out the colors from the color buffer
	  // into the vertexColor attribute.
	  {
	    this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, colorBuffer);
	    this.glContext.vertexAttribPointer(
	        this.programInfo.attributeLocations.vertexColor,
	        4,
	        this.glContext.FLOAT,
	        false,
	        0,
	        0);
	    this.glContext.enableVertexAttribArray(
	        this.programInfo.attributeLocations.vertexColor);
	  }

	  /* const textureCoordinates = [
		  1.0,  0.0,
		  0.0,  0.0,
		  1.0,  1.0,
		  0.0,  1.0,
	  ];
	  const textureBuffer = this.glContext.createBuffer();
	  this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, textureBuffer);
	  this.glContext.bufferData(this.glContext.ARRAY_BUFFER, new Float32Array(textureCoordinates), this.glContext.STATIC_DRAW);
	  // Tell WebGL how to pull out the texture coords from the texture coords buffer
	  // into the vertexColor attribute.
	  {
	    this.glContext.bindBuffer(this.glContext.ARRAY_BUFFER, textureBuffer);
	    this.glContext.vertexAttribPointer(
	        this.programInfo.attributeLocations.textureCoord,
	        2,
	        this.glContext.FLOAT,
	        false,
	        0,
	        0);
	    this.glContext.enableVertexAttribArray(
	        this.programInfo.attributeLocations.textureCoord);
	  }
 */



  }

  render(now /*:HighPrecisionNumber*/) {
  	now *= 0.001;
  	if (this.activeTimestamp != 0) {
  		this.rotation += now - this.activeTimestamp;
  		//this.rotation = 0;
  	}

  	this.activeTimestamp = now;
  	this.drawScene();
  	requestAnimationFrame(this.render.bind(this));

  }

  loadTextures(): Promise<void> {
		return new Promise<void>(resolve => {
	  	const image: any = new Image();

	  	image.onload = () => {
	  		const texture = this.glContext.createTexture();
		  	this.glContext.bindTexture(this.glContext.TEXTURE_2D, texture);
		  	this.glContext.texImage2D(this.glContext.TEXTURE_2D, 0, this.glContext.RGBA,
		    	this.glContext.RGBA, this.glContext.UNSIGNED_BYTE, image);

		    if (this.textureIssPowerOf2(image)) {
		    	this.glContext.generateMipmap(this.glContext.TEXTURE_2D);
		    } else {
		    	this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_S, this.glContext.CLAMP_TO_EDGE);
		    	this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_WRAP_T, this.glContext.CLAMP_TO_EDGE);
		    	this.glContext.texParameteri(this.glContext.TEXTURE_2D, this.glContext.TEXTURE_MIN_FILTER, this.glContext.LINEAR);
		    }

				resolve();
	  	}

	  	image.src = "/assets/discord.jpg";
		})
  }

  textureIssPowerOf2(value:HTMLImageElement): boolean {
  	console.log("WHEEEE", value);
  	const widthIsPowerOfTwo:boolean = (value.width & (value.width - 1)) == 0;
  	const heightIsPowerOfTwo:boolean =  (value.height & (value.height - 1)) == 0;

  	return widthIsPowerOfTwo && heightIsPowerOfTwo;
  }

}
