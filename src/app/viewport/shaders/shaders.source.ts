
export interface ShaderInfo {
	/*
	since the code only has access to the glEnum type where a WebGLRenderingContext is avaiable,
	the type here will be a const string value that can be used as a key for the glEnum type.
	example: "glContext[ShadersList.VERTEX_DEFAULT.type]"
	*/
	type: string, //glEnum in the kronos docs
	source: string,
	attributes? : {[key : string]: string}, //names of attribute variables used in program, keyed by reference names to be used in js program info. optional
	uniforms? : {[key : string]: string} //name of uniforms used in program, keyed by reference names to be used in js program info. optional
}

export enum ShaderName {
	VERTEX_DEFAULT = "vertex.shader.default",

	FRAGMENT_DEFAULT = "vertex.fragment.white"
}

export const ShadersList: {[key: string]: ShaderInfo } = {
	VERTEX_DEFAULT : {
		type: "VERTEX_SHADER",
		attributes: {
			vertexPosition: 'aVertexPosition',
			//vertexColor: 'aVertexColor',
			textureCoord: 'aTextureCoord',
		},
		uniforms: {
			modelViewMatrix: 'uModelViewMatrix',
			projectionMatrix: 'uProjectionMatrix',
		},
		source:  `
		attribute vec4 aVertexColor;
    attribute vec4 aVertexPosition;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying lowp vec4 vColor;
    

    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vColor = aVertexColor;
      
    }
`
	},
	FRAGMENT_DEFAULT: {
		type: "FRAGMENT_SHADER",
		uniforms: {
			sampler: 'uSampler'
		},
		source: `
		varying lowp vec4 vColor;
		//varying highp vec2 vTextureCoord;

		uniform sampler2D uSampler;

    void main() {
      gl_FragColor = vColor;
    }
`
	}
}