import {
	Component,
	ElementRef,
  ViewChild,
  AfterViewInit,

} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppState } from './app.state';
import { SetContext, SetError } from './WebGL/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

	@ViewChild("canvasEl") canvas: ElementRef;

	private subscriptions: Subscription[];
	loading: boolean;
	context: WebGLRenderingContext;
	err: string;


	constructor(
		private store: Store<AppState>,
	) {
		this.subscriptions = [
			store.pipe(select('webgl', 'context')).subscribe((ctx) => {this.context = ctx}),
			store.pipe(select('webgl', 'error')).subscribe((e) => {this.err = e}),
		];

		this.loading = true;
	}


	ngAfterViewInit() {
    const glContext: WebGLRenderingContext = this.canvas.nativeElement.getContext("webgl");
    this.loading = false;

    if (glContext != null) {
    	this.store.dispatch(new SetContext(glContext))
    } else {
    	this.err = "Unable to initialize WebGL. Your browser or machine may not support it.";
    }
	}
}
