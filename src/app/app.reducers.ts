import { ActionReducerMap } from '@ngrx/store';
import { AppState } from './app.state'
import { reducer as WebGLReducer } from './WebGL/reducer';

export const reducers: ActionReducerMap<AppState> = {
	webgl: WebGLReducer
}