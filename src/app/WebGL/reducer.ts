import { Action } from '@ngrx/store';
import {
	WebGLActions,
	WebGLActionTypes,
	SetContext,
	SetError
} from './actions';

export interface State  {
	context: WebGLRenderingContext | null,
	error?: string
}

const defaultState:State = {
	context: null
}

export function reducer(state: State = defaultState, action: Action) {
	if (action instanceof SetContext) {
		return {
			...state,
			context: action.payload
		};
	}

	if (action instanceof SetError) {
		return {
			...state,
			error: action.payload,
		}
	}

	return state;
}